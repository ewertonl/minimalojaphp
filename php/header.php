<header id="header">
    <nav class="navbar navbar-light">
        <div>
        <a href="index.php" class="navbar-logo">
            <img src="./img/logo.svg" alt="Minima Loja Logo" class="navbar-logo-img">
        </a>
        </div>

        <div class="navbar-cart">
            <a href="carrinho.php">
                <img src="./img/cart-count.svg" alt="Carrinho">
            </a>
            <?php
            
            if(isset($_SESSION['carrinho'])){
                $cartcount = count($_SESSION['carrinho']);
                echo "<span class=\"navbar-cart-counter\">$cartcount</span>";
            }else{
                echo "<span class=\"navbar-cart-counter\">0</span>";
            }
            ?>
        </div>
    </nav>
</header>