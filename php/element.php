<?php
// imprime um produto especificado
function product($product_id, $product_name, $product_description, $product_price,$product_price_discount, $product_image){
    $element = "
    
    <div class=\"item\" id=\"$product_id\">
        <form action=\"index.php\" method=\"POST\">
            <div class=\"card\">
                <div>
                    <img src=\"$product_image\" alt=\"Imagem do produto $product_name\" class=\"card-image-top\">
                </div>
                <div class=\"card-body\">
                    <h4 class=\"card-title\">$product_name</h4>
                    <p class=\"card-text\">
                        $product_description
                    </p>
                    <h4>
                        <small><s><span class=\"price text-secondary\">".number_format($product_price_discount, 2, ',', '.')."</span></s></small>
                        <span class=\"price\">".number_format($product_price, 2, ',', '.')."</span>
                    </h4>
                    <button type=\"submit\" name=\"add\" class=\"button button-primary\">Adicionar <i class='icon-cart-add'></i></button>
                    <input type=\"hidden\" name=\"product_id\" value=\"$product_id\">
                </div>
            </div>
        </form>
    </div>
    
    ";
    echo $element;
}

// imprime um produto especificado no formato do carrinho
function cartProduct($product_name, $product_image, $product_price, $product_id){
    $element = "
    
    <div class=\"cart-product margin-1r\">
            <form action=\"carrinho.php?action=remove&id=$product_id\" method=\"POST\" class=\"cart-item\">
                <div class=\"cart-item-img\">
                    <img src=\"$product_image\" alt=\"Produto No Carrinho: $product_name\" class=\"cart-item-img\">
                </div>
                <div class=\"flex-column\">
                    <div class=\"cart-item-body\">
                        <div class=\"cart-item-body-title\">
                            <h4>$product_name</h4>
                            <h4><span class=\"price\">".number_format($product_price, 2, ',', '.')."</span></h4>
                        </div>
                        <div class=\"cart-item-body-quantity\">
                            <button type=\"button\" class=\"cart-item-body-quantity-plus button\" onclick=\"plusOne('$product_id')\"><i class='icon-plus'></i></button>
                            <input type=\"text\" class=\"cart-item-body-quantity-field\" readonly=\"readonly\" value=\"1\" id=\"$product_id\">
                            <button type=\"button\" class=\"cart-item-body-quantity-minus button\" onclick=\"minusOne('$product_id')\"><i class='icon-minus'></i></button>
                        </div>
                    </div>
                    <div class=\"cart-item-buttons\">
                        <button class=\"button text-secondary\" type=\"submit\">Comprar depois</button>
                        <button class=\"button text-warning\" type=\"submit\" name=\"remove\">Remover <i class='icon-cart-remove text-warning'></i></button>
                    </div>
                </div>
            </form>
        </div>

    ";
    echo $element;
}
