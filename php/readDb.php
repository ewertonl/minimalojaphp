<?php

/* retorna os dados da tabela produtos no banco de dados como um array
*  $produto[n]['coluna'] => valor da coluna
*/
function getProducts(){
    $pdo = new PDO('sqlite:./db/minimaloja.db');
    $statement = $pdo->query("select * from produtos");
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}
?>
