<?php
// inicia a sessão do php
session_start();

// carrega os módulos necessários
require_once('./php/readDb.php');
require_once('./php/element.php');

// remove itens do carrinho
if(isset($_POST['remove'])){
// quando o botão de remover for acionado ->
    if ($_GET['action'] == 'remove') {
    // se a ação do método GET especificado no action
    // dos forms de cada item do carrinho for 'remove' ->
        foreach($_SESSION['carrinho'] as $key => $value){
        // para cada item no carrinho ->
            if($value['product_id'] == $_GET['id']){
            // se o ID do produto no array do carrinho
            // for igual ao ID no form do produto da página carrinho, o remova do array carrinho
                unset($_SESSION['carrinho'][$key]);
            }
        }
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Meu Carrinho</title>
    <link rel="stylesheet" href="./style/style.css">
</head>
<body>
<?php
    require_once('./php/header.php');
?>
<div class="container">
    <div class="cart-title">
        <h4>Meu Carrinho</h4>
    </div>
    
    <div class="items-wrapper left flex-column">
        <?php

        //cartProduct('Lenovo Vibe K5+', './upload/nosteponsnek.png', 299.90); // função cartProduct() com seus argumentos
        $total = 0;
        if(isset($_SESSION['carrinho'])){
            $product_id = array_column($_SESSION['carrinho'], 'product_id');

            foreach($product_id as $id){
                foreach(getProducts() as $item => $product){
                    if($product['id'] == $id){
                        //print_r($product['nome']."<br>"); // testando se o loop de validação funciona

                        cartProduct($product['nome'], $product['img'], $product['preco'], $product['id']);
                        $total = $total + (double)$product['preco'];
                    }
                }
            }
        }else{
            echo "<h5>Carrinho Vazio</h5>";
        }
        ?>
    </div>

    <div id="details">
        <h5>Total 
        <?php
        if(isset($_SESSION['carrinho'])){
            $count = count($_SESSION['carrinho']);
            echo "(".$count.") itens";
        }else{
            echo "";
        }
        ?>
        </h5>
        <h5><span class="price">
            <?php
            echo number_format($total, 2, ',','.');
            ?>
        </span></h5>
    </div>

    <!--Teste rápido com js-->
    <script>
        // adiciona +1 ao contador, atualmente é estética, não funcional
        function plusOne(id){
            const actualValue = parseInt(document.getElementById(`${id}`).value);
            document.getElementById(`${id}`).value = actualValue + 1;
            return false;
        }
        // Subtrai -1 ao contador, atualmente é estética, não funcional
        function minusOne(id){
            const actualValue = parseInt(document.getElementById(`${id}`).value);
            if (actualValue > 1) {
                document.getElementById(`${id}`).value = actualValue - 1;
            }
            return false;
        }
    </script>
</div>
</body>
</html>
