<?php

// inicia a sessão do php session()
session_start();

// carrega a o módulo de banco de dados, sqlite3 no caso usando PDO
// só tem uma função por enquanto e não criei classes
require_once('./php/readDb.php');

// carrega o módulo com funções de imprimir elementos
require_once('./php/element.php');


// isso tudo aqui poderia estar numa função, mas por hora fica assim por questões de testes rápidos
if(isset($_POST['add'])){
// quando o botão de adicionar for acionado ->
    if(isset($_SESSION['carrinho'])){
    // se a sessão estiver iniciada captura os IDs dos produtos:
        $item_array_id = array_column($_SESSION['carrinho'], 'product_id');
        //print_r($item_array_id);

        if(in_array($_POST['product_id'], $item_array_id)){
        // se o ID (produto) já existir na lista de itens adicionados ao carrinho, o remova
        
            echo "<script>console.log('Item removido do carrinho')</script>";
            //unset($_SESSION['carrinho'][$item_array_id]); // assim não funciona TODO foreach()
            foreach($_SESSION['carrinho'] as $key => $value){
            // funcionou, vai ficar assim por enquanto
                if($value['product_id'] == $_POST['product_id']){
                    unset($_SESSION['carrinho'][$key]);
                }
            }
        }else{
        // se não existir o ID do produto, o adicione ao array de intes no carrinho
            $count = count($_SESSION['carrinho']);
            $item_array = array(
                'product_id' => $_POST['product_id']
            );
            $_SESSION['carrinho'][$count] = $item_array;
        }

    }else{
    // se a sessão não tiver sido iniciada, crie o array de itens e inicialize a sessão com o array
        $item_array = array(
            'product_id' => $_POST['product_id']
        );
        // cria e inicializa a variável $_SESSION
        $_SESSION['carrinho'][0]=$item_array;
    }  
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="manifest" href="./manifest.webmanifest">
    <title>MinimaLoja</title>
    <link rel="stylesheet" href="./style/style.css">
</head>
<body>
    <?php
    require_once('./php/header.php');
    ?>
    <div class="container">
        <div class="items-wrapper">
            <?php
            // lê e imprime todos os itens retornados do banco de dados
            foreach(getProducts() as $item => $product){
                product($product['id'],$product['nome'], $product['descricao'], $product['preco'], $product['desconto'], $product['img']);
            }
            ?>
        </div>
    </div>

    <script>
        // adiciona um outline (classe css 'added-to-cart'para cada item adicionado ao carrinho
        // também muda o icone e o texto do botão de cada item marcado
        function highlightMarkedItems(){
            <?php
                if(isset($_SESSION['carrinho'])){
                    foreach($_SESSION['carrinho'] as $key){
                        print_r("document.getElementById('".$key['product_id']."').classList.toggle('added-to-cart');");
                        print_r("document.getElementById('".$key['product_id']."').children[0][0].innerHTML='Remover <i class=\'icon-cart-remove\'></i>';");
                    }
                }
            ?>
        }
        
        // chama a função highlightMarkedItems()
        highlightMarkedItems();
    </script>
</body>
</html>
