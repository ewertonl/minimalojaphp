# Minima Loja PHP

Fazendo uma loja usando PHP (e um pouco de Javascript para algumas funções interativas). Esta versão possui uma estilização básica para mobile.

**Desktop**

![MinimaLoja Desktop](./docs/img/image-1.jpg "MinimaLoja Desktop")

**Mobile**

![Minima Loja Mobile](./docs/img/image-2.jpg "Minima Loja Mobile")

## Antes de começar

#### Versão do PHP

Este projeto utiliza a versão *7.4.3* do PHP, talvez não funcione em versões anteriores.

#### Banco de dados

Como esse projeto utiliza um banco de dados **SQLite3**, talvez você precise habilitar o módulo sqlite3 nas suas configurações do PHP (php.ini)

##### XAMPP

Se estiver usando XAMPP, para habilitar o SQLite3, abra `xampp/php/php.ini`, descomente a linhas `;extension=sqlite3` e `;extension=pdo_sqlite` (tire o `;`), salve o arquivo `php.ini` e reinicie  o servidor.

##### Linux

Mesmos passos como o de cima, use o comando `locate php.ini` e descomente as linhas `;extension=sqlite3` e `; extension=pdo_sqlite`

Também verifice se o pacote **php-sqlite3** e **php7.4-sqlite3** está instalado na sua distribuição. O nome do pacote muda de acordo com a versão do php que está instalado, para saber a sua versão use o comando `php -v` 

## Estrutura de diretórios do projeto

- [/](#)
  
  - [/db/](#db)
    
    - [minimaloja.db](#db)
  
  - [/font/](#fontes)
  
  - [/img/](#img)
  
  - [/php/](#php)
    
    - [element.php](#element)
    
    - [header.php](#header-el)
    
    - [readDb.php](#readdb)
  
  - [/style/](#styles)
    
    - fontello.css
    
    - style.scss
  
  - [/upload/](#upload)
  
  - [/carrinho.php](#carrinho)
  
  - [/index.php](#index-project)
  
  - [/README.md](#this)
  
  - [sqlite3version.php](#sqlite3ver)

<h3 id="user-content-db">
Banco de dados
</h3>

O banco de dados SQLite3 do projeto possui a seguinte tabela:

**produtos**

| id                  | nome            | descricao                      | preco                  | desconto           | img                          |
| ------------------- | --------------- | ------------------------------ | ---------------------- | ------------------ | ---------------------------- |
| identificador único | nome do produto | uma breve descrição do produto | preço final do produto | preço sem desconto | caminho da imagem do produto |

<h3 id="user-content-fontes">
    Fontes
</h3>

Fontes utilizadas:

- Roboto
  
  > ![Roboto Font](./docs/img/image-3.jpg)

- Fontello (ícones customizados)
  
  > ![Ícones do site](./docs/img/image-4.jpg)

<h3 id="user-content-img">
    Imagens
</h3>

`/img/` Diretório onde será armazenado as imagens únicas da loja.

<h3 id="user-content-php">
    PHP
</h3>

<h4 id="user-content-element">
    Elementos
</h4>

Arquivo `/php/element.php`

Contém os elementos da loja com as seguintes funções:

```php
function product($_id, $_name, $_description, $_price, $_discount, $_image){
    $element = "...";
    echo $element;
}

function cartProduct($_name, $_image, $_price, $_id){
    $element = "...";
    echo $element;
}
```

<div id="user-content-header-el">

</div>

Arquivo `/php/header.php`

Contém o *header* da loja:

```html
<nav>
    <div>
        <a><img>Logo</img></a>
    </div>
    <div>
        <a><img>Carrinho</img></a>
    </div>
<nav>
```

<h4 id="user-content-readdb">
    Banco de dados readDb.php
</h4>

Arquivo `/php/readDb.php`. Contém a função:

```php
function getProducts(){
    $pdo = new PDO('sqlite:'...);
    $statement = $pdo->query("select * from produtos");
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}
```

Essa função retorna um *array* com as colunas da tabela *produtos*

<h3 id="user-content-styles">Estilo</h3>

Na pasta `/style/` há duas folhas de estilo importantes:

- style.scss

- fontello.css

O arquivo SASS `style.scss` é onde está a maior parte da estilização da loja.

O arquivo CSS `fontello.css` é o arquivo dos ícones customizados.

<h3 id="user-content-upload">
Upload
</h3>

Na pasta `/upload/` fica todos os arquivos enviados para a loja, como imagens dos produtos.

<h3 id="user-content-carrinho">
    Carrinho
</h3>

Arquivo `./carrinho.php`

Neste arquivo está contido a página de carrinho da loja.

<h3 id="user-content-index-project">
    Index.php
</h3>

Página inicial da minimaloja

<h3 id="user-content-sqlite3ver">
    Versão do SQLite3
</h3>

O arquivo `./sqlite3version.php` imprime na tela a versão atual do SQLite3

<h3 id="user-content-this">
    README
</h3>

Basicamente este documento
